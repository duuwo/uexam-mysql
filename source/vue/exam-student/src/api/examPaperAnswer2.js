import { post } from '@/utils/request'

export default {
  answerSubmit: form => post('/api/student/question/answerSubmit', form)
}
