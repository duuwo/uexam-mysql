package com.alvis.exam.excle.vm;

import java.util.HashMap;
import java.util.Map;


public class ExcelDealFactory {
	
	private static Map<String, AbstratExcelUtil<?>> excelUtil = new HashMap<>();
	public static AbstratExcelUtil<?> getExcelUtil(Class<? extends AbstratExcelUtil<?>> clazz){
		if(excelUtil.containsKey(clazz.getName())){
			return excelUtil.get(clazz.getName());
		}
		try {
			synchronized (clazz) {
				if(excelUtil.containsKey(clazz.getName())){
					return excelUtil.get(clazz.getName());
				}
				AbstratExcelUtil<?> o = clazz.newInstance();
				excelUtil.put(clazz.getName(), o);
				return o;
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
