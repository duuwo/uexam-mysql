package com.alvis.exam.excle.vm;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.alvis.exam.domain.enums.QuestionTypeEnum;
import com.alvis.exam.viewmodel.admin.question.QuestionEditItemVM;
import com.alvis.exam.viewmodel.admin.question.QuestionEditRequestVM;

public class QuestionExcelUtil extends AbstratExcelUtil<QuestionEditRequestVM> {

	private int unit = 3;

	@Override
	public List<QuestionEditRequestVM> parseInteral(Workbook workbook) {
		// 得到一个工作表
		Sheet sheet = workbook.getSheetAt(0);
		// 获取行总数
		int rows = sheet.getLastRowNum()+1;
		if (rows < unit) {
			throw new RuntimeException("格式错误,每"+unit+"行为一个处理单元");
		}

		List<QuestionEditRequestVM> list = new ArrayList<>();
		Row[] rowArray = new Row[unit];
		for (int i = 0; i < rows; ) {
			for (int j = 0; j < unit; j++) {
				rowArray[j] = sheet.getRow(i++);
			}
			QuestionEditRequestVM vm = new QuestionEditRequestVM();
			// question_type
			String question_type = getCellValueToString(rowArray[0].getCell(0));
			if ("单选题".equals(question_type)) {
				vm.setQuestionType(QuestionTypeEnum.SingleChoice.getCode());
				List<QuestionEditItemVM> items = getItems(rowArray[1]);
				if (items == null || items.size() == 0) {
					throw new RuntimeException("格式错误,没有答案选项");
				}
				vm.setItems(items);
			} else if ("多选题".equals(question_type)) {
				vm.setQuestionType(QuestionTypeEnum.MultipleChoice.getCode());
				List<QuestionEditItemVM> items = getItems(rowArray[1]);
				if (items == null || items.size() == 0) {
					throw new RuntimeException("格式错误,没有答案选项");
				}
				vm.setItems(items);
			} else if ("填空题".equals(question_type)) {
				vm.setQuestionType(QuestionTypeEnum.GapFilling.getCode());
			} else if ("简答题".equals(question_type)) {
				vm.setQuestionType(QuestionTypeEnum.ShortAnswer.getCode());
			} else {
				throw new RuntimeException("格式错误,问题类型错误");
			}
			// title
			String title = getCellValueToString(rowArray[0].getCell(1));
			vm.setTitle(title);
			// correct
			String correct = getCellValueToString(rowArray[0].getCell(2)).toUpperCase();
			vm.setCorrect(correct);
			
			// gradeLevel
			String gradeLevel = getCellValueToString(rowArray[0].getCell(3));
			vm.setGradeLevel(Integer.parseInt(gradeLevel));
			
			// subjectName
			String subjectName = getCellValueToString(rowArray[0].getCell(4));
			vm.setSubjectName(subjectName);

			// analyze
			String analyze = getCellValueToString(rowArray[2].getCell(0));
			vm.setAnalyze(analyze);

			// score
			String score = getCellValueToString(rowArray[0].getCell(5));
			if(StringUtils.isNotBlank(score)){
				vm.setScore(score);
			}else{
				vm.setScore("2");
			}
			
			// difficult
			String difficult = getCellValueToString(rowArray[0].getCell(6));
			if(StringUtils.isNotBlank(difficult)){
				vm.setDifficult(Integer.parseInt(difficult));
			}else{
				vm.setDifficult(3);
			}
			list.add(vm);
		}
		return list;
	}

	private List<QuestionEditItemVM> getItems(Row row) {
		List<QuestionEditItemVM> items = null;
		int c = row.getPhysicalNumberOfCells();
		System.out.println(c);
		int c2 = row.getLastCellNum();
		System.out.println(c2);
		if (c >= 2) {
			items = new ArrayList<>();
			for (int m = 0; m < c; ) {
				QuestionEditItemVM item = new QuestionEditItemVM();
				String prefix = getCellValueToString(row.getCell(m++)).toUpperCase();
				item.setPrefix(prefix);
				String content = getCellValueToString(row.getCell(m++));
				item.setContent(content);

				items.add(item);
			}
		}
		return items;
	}
	
	private String getCellValueToString(Cell cell) {
		if(cell==null){
			return null;
		}
		 cell.setCellType(CellType.STRING);
		 return cell.getStringCellValue();
	}

}
