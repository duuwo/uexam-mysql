package com.alvis.exam.service;

import java.util.List;

import com.alvis.exam.domain.Subject;
import com.alvis.exam.viewmodel.admin.education.SubjectPageRequestVM;
import com.github.pagehelper.PageInfo;

public interface SubjectService extends BaseService<Subject> {

    List<Subject> getSubjectByLevel(Integer level);

    List<Subject> allSubject();

    Integer levelBySubjectId(Integer id);

    PageInfo<Subject> page(SubjectPageRequestVM requestVM);

	Subject selectByLevelAndName(Integer gradeLevel, String subjectName);
}
