package com.alvis.exam.base;

import lombok.Data;

/**
 * @author alvis
 */

@Data
public class BasePage {
    private Integer pageIndex=1;

    private Integer pageSize=10;

}
