package com.alvis.exam.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.alvis.exam.domain.Subject;
import com.alvis.exam.viewmodel.admin.education.SubjectPageRequestVM;

@Mapper
public interface SubjectMapper extends BaseMapper<Subject> {
    int deleteByPrimaryKey(Integer id);

    int insert(Subject record);

    int insertSelective(Subject record);

    Subject selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Subject record);

    int updateByPrimaryKey(Subject record);

    List<Subject> getSubjectByLevel(Integer level);

    List<Subject> allSubject();

    List<Subject> page(SubjectPageRequestVM requestVM);

	Subject selectByLevelAndName(@Param("level")Integer level, @Param("name")String name);
}
