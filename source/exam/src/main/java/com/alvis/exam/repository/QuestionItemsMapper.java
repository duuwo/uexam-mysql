package com.alvis.exam.repository;

import com.alvis.exam.domain.QuestionItems;

public interface QuestionItemsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(QuestionItems record);

    int insertSelective(QuestionItems record);

    QuestionItems selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(QuestionItems record);

    int updateByPrimaryKey(QuestionItems record);
}