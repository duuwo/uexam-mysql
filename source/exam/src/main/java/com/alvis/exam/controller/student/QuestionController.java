package com.alvis.exam.controller.student;

import lombok.AllArgsConstructor;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alvis.exam.base.BaseApiController;
import com.alvis.exam.base.BasePage;
import com.alvis.exam.base.RestResponse;
import com.alvis.exam.domain.Question;
import com.alvis.exam.service.QuestionService;
import com.alvis.exam.viewmodel.student.question.answer.QuestionAnswerCommitVM;
import com.github.pagehelper.PageInfo;

@RestController("StudentQuestionController")
@RequestMapping(value = "/api/student/question")
@AllArgsConstructor
public class QuestionController extends BaseApiController {

    private final QuestionService questionService;

    //@PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/queryQuestions", method = RequestMethod.POST)
    public RestResponse<PageInfo<Question>> queryQuestions() {
    	BasePage page = new BasePage();
        PageInfo<Question> pageInfo = questionService.queryQuestions(page);
        return RestResponse.ok(pageInfo);
    }
    
    @RequestMapping(value = "/answerSubmit", method = RequestMethod.POST)
    public RestResponse<String> answerSubmit(@RequestBody QuestionAnswerCommitVM vm) {
    	questionService.answerSubmit(vm);
        return RestResponse.ok();
    }
    
}
