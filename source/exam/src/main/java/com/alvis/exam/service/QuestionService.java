package com.alvis.exam.service;

import com.alvis.exam.base.BasePage;
import com.alvis.exam.domain.Question;
import com.alvis.exam.viewmodel.admin.question.QuestionEditRequestVM;
import com.alvis.exam.viewmodel.admin.question.QuestionPageRequestVM;
import com.alvis.exam.viewmodel.student.question.answer.QuestionAnswerCommitVM;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface QuestionService extends BaseService<Question> {

    PageInfo<Question> page(QuestionPageRequestVM requestVM);

    Question insertFullQuestion(QuestionEditRequestVM model, Integer userId);

    Question updateFullQuestion(QuestionEditRequestVM model);

    QuestionEditRequestVM getQuestionEditRequestVM(Integer questionId);

    QuestionEditRequestVM getQuestionEditRequestVM(Question question);

    Integer selectAllCount();

    List<Integer> selectMothCount();

	PageInfo<Question> queryQuestions(BasePage page);

	void answerSubmit(QuestionAnswerCommitVM vm);

	void insertBatch(List<QuestionEditRequestVM> list);
}
