package com.alvis.exam.viewmodel.student.question.answer;

import java.io.Serializable;

import com.alvis.exam.viewmodel.admin.question.QuestionEditRequestVM;
import com.alvis.exam.viewmodel.student.exam.ExamPaperSubmitItemVM;

import lombok.Data;

@Data
public class QuestionAnswerCommitVM implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer questionId;
	
	private Boolean doRight;
	
}
